from django.shortcuts import render
from django.http import JsonResponse
import requests
import json
from .forms import Search_Box


# Create your views here.
def index(request):
    response = {
        'form': Search_Box
    }
    return render(request,'index.html',response)

def search_books(request, param):
    json = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + param).json()
    return JsonResponse(json)
