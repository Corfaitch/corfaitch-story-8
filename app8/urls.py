from django.urls import path
from . import views

app_name = 'app8'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('search_books/<str:param>/', views.search_books, name="search_books"),
]