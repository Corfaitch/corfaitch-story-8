from django import forms

class Search_Box(forms.Form):
    Search_Box = forms.CharField(label="Masukkan nama buku di sini", widget=forms.TextInput(attrs={'class': "form-control"}))
